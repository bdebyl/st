# Description
This is my fork of
the [Suckless Simple Terminal](https://st.suckless.org/). I've made some changes
to customize the terminal to work with my workflow. See the `patches/` folder to
see the original `.diff` patches used to apply the changes to the source files.

# Original README
----
st - simple terminal
--------------------
st is a simple terminal emulator for X which sucks less.


Requirements
------------
In order to build st you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install


Running st
----------
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.

Credits
-------
Based on Aurélien APTEL <aurelien.aptel@gmail.com> bt source code.

